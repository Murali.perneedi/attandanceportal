import React, { useState, useRef} from "react";
import Home from "../Homepage/Home";
import './index.css'
import {ImCross} from 'react-icons/im';
import {AiFillInfoCircle} from 'react-icons/ai';

const  VerifyCheck = () => {
    const password = useRef(null);
    const confirmPassword = useRef(null);
    const [error, setError] =useState(false);
    const [errmsg ,setMsg] = useState(false);
    const [lenErr, setLenEr] = useState(false);

    const isCheck = (event) =>{
      const passCheck = password.current.value
      const confirmCheck = confirmPassword.current.value
        event.preventDefault();

        if((passCheck === confirmCheck) && passCheck.length >=4 && confirmCheck.length >= 4){
            localStorage.setItem("passwordData", password.current.value)
            setError(!error)
            
        }if(Boolean(localStorage.getItem("userfirstattempt")) === true){
          localStorage.removeItem("userfirstattempt");
        }

    }
    
    const checkValues = () => {
      const passCheck = password.current.value
      const ConfirmCheck = confirmPassword.current.value

      if(passCheck.length < 4  && ConfirmCheck.length < 4){
        setLenEr(!lenErr);
      }if ((passCheck !== ConfirmCheck && (passCheck.length >=4 && ConfirmCheck.length>=4))){
        setMsg(!errmsg);
      }
    }

  return (
    <>
       { error ? <Home></Home>  :
        <div className="cont">
          <h1  >Sign IN Admin ?</h1>
        <form className="formValues" onSubmit={isCheck} >
        <div>
          <button className="btnicon" onClick={()=> setError(!error)}><ImCross/></button>
        </div>
        <h1 style={{color: "#ffffff"}}>Change Password</h1>
        <input ref={password} type="password" name="password" required='req' placeholder="Password"></input> <br/>
        <input  ref={confirmPassword} type="password"  required="req" name="confirmPassword" placeholder='Confirm Password'></input>  <br/>
       {lenErr ? <p className="errP">Length Error Enter Minimum 4 Length Pin <span><AiFillInfoCircle /></span></p> : ! errmsg !== true ? <p  className="errP">Error ! pin code doesn't match <span><AiFillInfoCircle /></span> </p>  :""}
        <button type="button" className="btnC" onClick={()=> setError(!error)}>Cancel</button>
        <button className="btnC2" onClick={checkValues}  type='submit'>Confirm</button>
        </form>
        </div>
       
        }
        
    </>
  )
}

export default VerifyCheck;