import React from "react";

const Pop =({editFormData,handleEditFormChange,handleCancelClick}) =>{
    return(
        <div className='popup-inner'>
            <input type="text" name="name" required="required"  value={editFormData.name}  onChange={handleEditFormChange}></input> <br/>
            <input type="text" name="role" required="required"  value={editFormData.role} onChange={handleEditFormChange}></input> <br/>
            <input type="mail" name="mail" required="required"  value={editFormData.mail}  onChange={handleEditFormChange}></input><br/>
           <button className='close-btn' handleCancelClick={handleCancelClick} >Cancel
           </button>
           <button className='btn' type='submit' >Save</button>
          </div>
    )
}

export default Pop;