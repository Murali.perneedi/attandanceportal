import React, { useState } from 'react'
import './index.css'
import Dashboard from '../dashBoard/index'
import {IoArrowBackOutline} from 'react-icons/io5'


const AdminTodo=  () => {
    // const [persons, setPerson] = useState(AttandanceList)

    const AttandanceList =[
      {
          id : 1,
          head:"Present",
          name1:"Saad Mushtaq",
          name2:"Zahid Butt",
          name3:"Ali Ahmad",
      },
      {
          id: 2,
          head:"OnLeave",
          name1:"Husain Magsood",
          name2:"Abdul Moeez",
          name3:"Abeer Faiz"
      },
      {
          id: 3,
          head:"Absent",
          name1:"Ayaz afzal",
          name2:"Zeeshan Sattar", 
          name3:"Bilal Mirza",
      },
    ]

    const[search, setSearch] = useState('');
    const [dash ,setDash] = useState(false);
    const [data, setData] = useState(AttandanceList)
    

    const handleChange = value =>{
      setSearch(value);
      filterData(value);
    }
    
    const filterData = value =>{
      const lowerCaseValue = value.toLowerCase().trim();
      if(!lowerCaseValue){
        setData(AttandanceList)
      }else{
        const filteredData = AttandanceList.filter(item =>{
          return Object.keys(item).some(key => {
            return item[key].toString().toLowerCase().includes(lowerCaseValue);
          })
        })
        setData(filteredData)
      }
    } 


  return (
    <div>
  {dash ? <Dashboard></Dashboard>:<div className='containers'>
    <button className='backBtn' onClick={()=> setDash(!dash) } type="button"> <IoArrowBackOutline/></button>
      <h1 style={{color:"black",}}>Today's Avaiabiliity</h1>
      <input placeholder='Search Here...' value={search} type="text" onChange={e => handleChange(e.target.value)}></input>   
      <div className='flexitem'>
           {data.map((person) => 
           <div className='list'>
            <button className='headTag'>
            {person.head}
            </button>
            <div key={person.id}>
                <p>{person.name1}</p>
                <p>{person.name2}</p>
                <p>{person.name3}</p>
            </div>
            </div>

           )}
           </div>
            {data.length === 0 && <span>No name Found</span>}
        </div>}
        </div>
  )
}



export default AdminTodo