import React, {  useState } from "react";
import{TiArrowSortedUp,TiArrowSortedDown} from 'react-icons/ti';
import './sort.css';
import Dashboard from '../dashBoard/index' ;
import {IoArrowBackOutline} from 'react-icons/io5';

var ListOfEmployes = [
    {
        id:1,
        name:"SaadMushatq",
        working:160,
        Average:8.0,
    },
    {
        id:2,
        name:"HausnaiMaqsood",
        working:150,
        Average:7.5,
    },
    {
        id:3,
        name:"AyazAfzal",
        working:140,
        Average:7.0,
    },
    {
        id:4,
        name:"ZeeshanSattar",
        working:120,
        Average:6.0,
    },
    {
        id:5,
        name:"AbdualMoez",
        working:160,
        Average:8.0,
    },
    {
        id:6,
        name:"ZahidButt",
        working:145,
        Average:7.25,
    },
    {
        id:7,
        name:"AliAhmed",
        working:120,
        Average:6.0,
    },
    {
        id:8,
        name:"AberFiaz",
        working:152,
        Average:7.60,
    },
]
const SortingTable = () => {

    const [users, setUser] = useState(ListOfEmployes);
    const [search, setSearch] = useState('');
    const [dash,setDash] =useState(false);
    const isSort  = 'ASC';
    const dwSort = 'DSC';
    
  
    
    const sorting = (col) =>{
        if(isSort === 'ASC'){
            const sorted = [...users].sort((a,b) =>
            a[col].toLowerCase() > b[col].toLowerCase() ? 1:-1
            );
            setUser(sorted);
        }
    }
    const dowmSorting = (col) =>{
        if(dwSort === 'DSC'){
            const dwsorted = [...users].sort((a,b) =>
            a[col].toLowerCase() < b[col].toLowerCase() ? 1:-1
            );
            setUser(dwsorted);
        }
    }


    return(
        <div>
       {dash? <Dashboard></Dashboard> :
       <div className="sort-container">
        <button className="backBtn" type="button" onClick={()=>setDash(!dash)}><IoArrowBackOutline/></button>
            <h1>Overall Stats</h1>
            <input placeholder="Search Here..." type={"text"} onChange={((event)=> setSearch(event.target.value))}></input><br/>
            <div className="btnSort">
            <button className="bt" onClick={()=> sorting("name")} ><span><TiArrowSortedUp/></span></button>
            <button className="bt" onClick={()=> dowmSorting("name")}><span><TiArrowSortedDown/></span></button>
            </div>
            <table className="content-table2">
                <thead>
                    <tr >
                    <th>Name</th>
                    <th>Total Hrs.</th>
                    <th>Dialy Average Hrs.</th>
                    </tr>
                </thead>
                <tbody>
                        {users.filter(user => user.name.toLowerCase().includes(search.toLowerCase())).map((user) =>
                        <tr>
                        <td>{user.name} </td>
                        <td>{user.working}</td>
                        <td>{user.Average}</td>
                         </tr>
                        )
                        }
                </tbody>
            </table>
        </div>}
        </div>
    )
}

export default SortingTable;
