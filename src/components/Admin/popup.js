import React from "react";
import './popup.css'
import {ImCross} from 'react-icons/im';
const PopUp = ({
  editFormData,
  handleEditFormChange,
  handleCancelClick,
}) => {
  return  (
    <div className="popContainer">
   <div>
    <button className="btnicon"><span onClick={handleCancelClick}><ImCross /></span></button>
   </div>
   <h1 className="popHead"  style={{color:"#ffffff"}}>Edit info</h1>
        <input
          type="text"
          required="required"
          placeholder="Enter a name..."
          name="name"
          value={editFormData.name}
          onChange={handleEditFormChange}
        />
     
        <input
          type="text"
          required="required"
          placeholder="Enter an address..."
          name="role"
          value={editFormData.role}
          onChange={handleEditFormChange}
        />
      
        <input
          type="email"
          required="required"
          placeholder="Enter an email..."
          name="mail"
          value={editFormData.mail}
          onChange={handleEditFormChange}
        />
     <div className="button-container">
     <button type="button" s className="editbtn1" onClick={handleCancelClick}>
          Cancel
        </button>
        <button  className='editbtn2' type="submit">Save</button>
        
        </div>
    </div>
  );
};

export default PopUp;
