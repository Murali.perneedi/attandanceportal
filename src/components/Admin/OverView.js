import React, {  useState } from 'react';
import './overview.css';
import {nanoid} from 'nanoid';
import PopUp from './popup';
import './overview.css'
import 'antd/dist/antd.css';
import Dashboard from '../dashBoard';
import {IoArrowBackOutline} from 'react-icons/io5'

var overList = [
    {
        id:1,
        name:"SaadMushtaq",
        role:"Fronted Engineer",
        mail:"saad@gmail.com",
        working:160,
        Avg:8.0,
    },
    {
        id:2,
        name:"HusnainMaqsood",
        role:"Fronted Engineer",
        mail:"husnain@gmail.com",
        working:150,
        Avg:7.5,
    },
    {
        id:3,
        name:"AyazAfzal",
        role:"Backend Engineer",
        mail:"ayaz@gmail.com",
        working:140,
        Avg:7.0,
    },
    {
        id:4,
        name:"ZeeshanSattar",
        role:"Hr Managerr",
        mail:"zeeshan@gmail.com",
        working:120,
        Avg:6.0,
    },
    {
        id:5,
        name:"AbdulMoeez",
        role:"Fronted Engineer",
        mail:"saad@gmail.com",
        working:160,
        Avg:8.0,
    },
    {
        id:6,
        name:"SaadMushtaq",
        role:"Backend Engineer",
        mail:"samg@gmail.com",
        working:160,
        Avg:8.0,
    },
]
const OverView = () => {
    const [contacts ,setContacts] = useState(overList);
    const [pop, setPop] = useState(false);
    const [search, setSearch] = useState('');
    const [data, setData] = useState(contacts)

    const [dash , setDash] =useState(false);
    const [editContactId, setEditContactId] = useState(null);
    const [addFormData, setFormData] = useState({
        name:'',
        role:'',
        mail:'',
        working:'',
        Avg:'',
    })
    const [editFormData, setEditFormData] = useState({
        name:'',
        role:'',
        mail:'',
        working:'',
        Avg:'',
      });

      const handleChange = value =>{
        setSearch(value);
        filterData(value);
      }

      const filterData = value =>{
        const lowerCaseValue = value.toLowerCase().trim();
        if(!lowerCaseValue){
          setData(contacts)
        }else{
          const filteredData = contacts.filter(item =>{
            return Object.keys(item).some(key => {
              return item[key].toString().toLowerCase().includes(lowerCaseValue);
            })
          })
          setContacts(filteredData)
        }
      } 
  
    

    const handleForm = (event) => {
        event.preventDefault();
        const filedName = event.target.getAttribute('name');
        const fieldcontact = event.target.value
        const newFromData = {...addFormData};
        newFromData[filedName] = fieldcontact;
        setFormData(newFromData)
    }
    const handleSubmit = (event) =>{
        event.preventDefault();
        const newcontact = {
            id: nanoid(),
            name: addFormData.name,
            role: addFormData.role,
            mail: addFormData.mail,
            working: addFormData.working,
            Avg: addFormData.Avg,
        }
        const newcontacts = [...contacts, newcontact]
        setContacts(newcontacts);
    }

    const handleEditFormChange = (event) => {
        event.preventDefault();
        const fieldName = event.target.getAttribute("name");
        const fieldcontact = event.target.value;
        const newFormData = { ...editFormData };
        newFormData[fieldName] = fieldcontact;
        setEditFormData(newFormData);
      };
      
    const handleEditClick = (event, contact) => {
        event.preventDefault();
        setPop(!pop);
        setEditContactId(contact.id);
        const formValues = {
        name: contact.name,
        role: contact.role,
        mail: contact.mail,
        working:contact.working,
        Avg:contact.Avg,
        };       
    setEditFormData(formValues);
    };
        
    const handleCancelClick = () => {
        setEditContactId(null);
        setPop(!pop);
      };
    
    const handleEditFormSubmit = (event) => {
        event.preventDefault();
        const editedContact = {
          id: editContactId,
          name: editFormData.name,
          role: editFormData.role,
          mail: editFormData.mail,
          working: editFormData.working,
          Avg: editFormData.Avg,
        };
        const newContacts = [...contacts];
        const index = contacts.findIndex((contact) => contact.id === editContactId)
        newContacts[index] = editedContact;    
        setContacts(newContacts);
        setPop(!pop);
      };
      
      const handleDeleteClick = (contactId) => {
        const newContacts = [...contacts];
    
        const index = contacts.findIndex((contact) => contact.id === contactId);
        const valuuendex = contacts.findIndex((data) => data.id);
        console.log(valuuendex);
        newContacts.splice(index, 1);
    
        setContacts(newContacts);
       
      }

  return (
    <div>
   {dash ? <Dashboard></Dashboard>: <div className={pop ?'containerSetting':''}>
        <button className='backBtn' onClick={()=> setDash(!dash)}><IoArrowBackOutline/></button>
        <h1>Setting</h1>
        <input className='inpt' placeholder='Search Here...' value={search} type="text" onChange={e => handleChange(e.target.value)}></input>
        <form  onSubmit={handleEditFormSubmit} >
        <table className='content-table3'>
            <thead >
                <tr>
                <th>Home</th>
                <th>Position</th>
                <th>Email</th>
                <th>Total Hrs.</th>
                <th>Dialy Average Hits</th>
                <th>Action</th>
                </tr>
            </thead>
            <tbody>
                {contacts.map((contact) => 

                 <tr key={contact.id}>
                 <td>{contact.name}</td>
                 <td>{contact.role}</td>
                 <td>{contact.mail}</td>
                 <td>{contact.working}</td>
                 <td>{contact.Avg}</td>
                 <td type='button' onClick={() => handleDeleteClick(contact.id)} className="dltBtn" >Delete</td>
                 <td type='button' className='editBtn' editFormData={editFormData} onClick={(event) => handleEditClick(event, contact) }>Edit</td>
                 </tr>
                )}
            </tbody>
        </table>
        {pop ?
        <PopUp   editFormData={editFormData} handleCancelClick={handleCancelClick}
                    handleEditFormChange={handleEditFormChange} 
                 />:""}
        </form>
        
        
        <form onSubmit={handleSubmit}>
        <h1>Add Employee</h1>
        <span style={{color: "#00bfa6",padding: 2 }}>First Name</span><input type="text" name="name" required="required" placeholder='Name' onChange={handleForm} />
        <span style={{color: "#00bfa6",padding: 2 }}>Last Name</span><input type="text" name="name" required="required" placeholder='Name' onChange={handleForm} />
        <span style={{color: "#00bfa6",padding: 2}}>Dept</span><input type="text"  placeholder='Dept'></input>
        <span style={{color: "#00bfa6",padding: 2}}>Position</span><input type="text" name="role" required="required" placeholder=' Position' onChange={handleForm} />
        <span style={{color: "#00bfa6",padding: 2}}>Email</span><input type="mail" name="mail" required="required" placeholder=' Email' onChange={handleForm} /><br/>
            <button className='inputButton' type='submit'>Add</button>
        </form>
        
        <form className='change' onSubmit={handleSubmit}>
        <h1>Change office Hours</h1>
        <span style={{color: "#00bfa6",padding: 25 }}>Start Time</span><input type="text" name=""  placeholder='9:00 AM' onChange={handleForm} />
        <span style={{color: "#00bfa6",padding: 25 }}>Finsh Time</span><input type="text" name="" placeholder='6:00 PM' onChange={handleForm} />
        <span style={{color: "#00bfa6",padding: 25 }}>Work Hr</span><input type="mail" name="Avg" required="required" placeholder=' 8' onChange={handleForm} />
            <br/>
            <button className='inputButton' type='submit'>Change</button>
        </form>
        {data.length === 0 && <span>No name Found</span>}
        </div>}
        </div>
  )
}

export default OverView;