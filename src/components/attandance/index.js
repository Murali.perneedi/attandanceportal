import React, {useState} from 'react'
import "./index.css"
import Dashboard from '../dashBoard'
import {BiX} from 'react-icons/bi'
var data = [
    {
        id:1,
        Date:"03/03/2022",
        Status:"Present",
    },
    {
        id:2,
        Date:"04/03/2022",
        Status:"Absent",
    },
    {
        id:3,
        Date:"05/03/2022",
        Status:"Present",
    },
    {
        id:4,
        Date:"06/03/2022",
        Status:"Leave",
    },
    {
        id:5,
        Date:"07/03/2022",
        Status:"Present",
    },
    {
        id:6,
        Date:"08/03/2022",
        Status:"Absent",
    },
]

const Attandance = () => {
    const [details, setDetails] = useState(data);
    const [cancel, isCanceled] = useState(false);
    const[loggin , setLoggin] =useState(false);
    const [search, setSearch] = useState('');

    console.log(setDetails)

    const onClick = () => {
        isCanceled(!cancel)
        localStorage.setItem("userfirstattempt", JSON.stringify(setLoggin(!loggin)));
    }


  return (
    <div className={cancel ? "":"record-Container"}>
    { cancel ?  <Dashboard> </Dashboard> :
    <div >
    <div className='userContainer'>
        <div className='alignBtn'>
        <button className='userBtn' type='button' onClick={onClick}><span><BiX/></span></button>
        </div>
        <div className='content'>
            <h7 className='userH' style={{ color: "#ffffff"}}>Attendance Recorder</h7>
            <p3>Saad Mushtaq</p3>
            </div>
            <input text="text" value={search}  onChange={(e) => setSearch(e.target.value)} placeholder='Search By Date...'></input><br/>
                <table className='content-table' >
                <thead >
                    <tr >
                        <th>Date</th>
                        <th>Status</th>
                    </tr>
            </thead>
        <tbody>
        {details.filter(detail => detail.Date.includes(search)).map((detail)=>
            <React.Fragment  key={detail.id}>
            <tr>
            <td>{detail.Date}</td>
            <td>{detail.Status}</td>
            </tr>
        </React.Fragment>
            )}
            </tbody>
            </table>
            </div>
            </div>
}         
            </div>
           
  )
}

export default Attandance;