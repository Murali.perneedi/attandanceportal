import React, {useState} from "react";
import Attandance from "../attandance";
import AdminTodo from "../Admin/AdminTodo";
import SortingTable from "../Admin/SortingTable";
import OverView from "../Admin/OverView";
import img from './img.jpeg';
import './index.css';
import {IoSettingsSharp,IoArrowBackOutline} from 'react-icons/io5'
import Home from '../Homepage/Home'
import {FaSignOutAlt} from 'react-icons/fa'
import BounceLoader from 'react-spinners/BounceLoader'

const Dashboard = () => {
const [isClicked ,setClicked] = useState(false);
const [Leave, setLeave] = useState(false);
const values = Boolean(localStorage.getItem("Admin"));
const [adminTodo , setTodo] = useState(false);
const [adminoverall , setAdmin] = useState(false);
const [over ,setover] = useState(false);
const [logout, setLogout ] = useState(false);
const [attend, setAttend] = useState(false);
const [spinner, setSpinner] = useState(false);

    const userRecord = () => {
      setClicked(!isClicked);
    }
    const adminStats = () => {
      setAdmin(!adminoverall);
    }
    const applyLeave = () =>{
      setLeave(!Leave);
    }
     const adminToday = () =>{
      setTodo(!adminTodo);

     }
    const userToday = () => {
      setAttend(!attend);
    }
    const setting = () => {
      setover(!over);
    }
    const logoutClick = () => {
      setSpinner(!spinner);
        setTimeout(()=>{
          setSpinner(false);
          console.log("hi");
        },1000
        )
     setLogout(!logout);
    }
    

  return (
<div>
    {spinner ? <BounceLoader color="#65f1d6"  cssOverride={{marginTop:350,marginLeft:700}}/> :logout ? <Home></Home> : 
    <div className="dashContainer">
     
      {over ? <OverView></OverView> :adminoverall ? <SortingTable> </SortingTable> : adminTodo ? <AdminTodo></AdminTodo> 
      :attend?<div><h1>Attandance Approved</h1> <button className='backBtn' onClick={()=> setAttend(!attend)} type="button" ><IoArrowBackOutline/></button></div> 
      :Leave?  <div><h1>Your Leave's Are Over</h1><button className='backBtn'  onClick={()=> setLeave(!Leave) } type="button"><IoArrowBackOutline/></button></div> 
      :isClicked ? <Attandance></Attandance> : <div>
        {values ?  <div className="iconS"><button type="button" className="adminicon" onClick={setting}><span ><IoSettingsSharp /></span></button> </div>: null}
        <img className="logo" src = {img} alt="logo" />
        <h1>Hi! saad Mushtaq</h1>
        <div>
        <button type="button" className="userbtn" onClick={values ? adminToday:userToday  }>{ values ? "Today's Availability":"PUNCH  ATTEENDANCE"}</button>
       {values? null:<button  className = "userbtn"  type="button"  onClick={applyLeave}>APPLY FOR LEAVE</button>}
        <button type="button"  className="userbtn" onClick={ values ? adminStats:userRecord}>{values ? "Overall Stats": "WATCH PREVIOUS RECORDS"}</button>
        <div className="btnLogout">
          <button type="button"  onClick={logoutClick}>
            <FaSignOutAlt/>
          </button>
        </div>
        </div>
        </div>}
        </div>
  }
  </div>

  )
}

export default Dashboard;