import React,{useRef, useState} from 'react'
import VerifyCheck from '../updatePassword';
import Dashboard from '../dashBoard';
import './index.css';
import { FaUserAlt,FaLock } from 'react-icons/fa'
import {TiSocialFacebook} from 'react-icons/ti'
import {RiTwitterFill} from 'react-icons/ri'
import {AiOutlineGoogle,AiFillInfoCircle} from 'react-icons/ai'
import {TiSocialLinkedin} from 'react-icons/ti'
import ClipLoader  from 'react-spinners/ClipLoader';

const Home = () => {
    const [user ,setUser] =  useState(false);
    const [dash ,setDash] =  useState(false);
    const [isLogin, setLogin] = useState(false);
    const username = useRef(null);
    const password = useRef(null);
    const [admin , setAdmin] = useState(false);
    const [errorMsg, setError] =useState(false);
    const[spinner , setSpinner] = useState(false);
    

    function isUser () {
        setUser(!user);
    }

    

    
    const isVerified = e => {
        e.preventDefault();

        

       const userid = username.current.value;
       const idName = userid.slice(0,2);
       const idNum = (userid.slice(-3,));
       const pass = password.current.value;
       
      //  localStorage.getItem("passwordData") === pass 
       if (user){
        if((userid === "admin") && (pass === "admin")){
          setSpinner(!spinner);
          setTimeout(()=>{
            setSpinner(false);
            console.log("hi");
          },1000
          )
         setAdmin(!admin);
        }
    }

       if((localStorage.getItem("usernameData") === userid && userid !== "admin " && user !== true)  && (localStorage.getItem("passwordData") === pass ) ){
        setSpinner(!spinner);
        setTimeout(()=>{
          setSpinner(false);
          console.log("hi");
        },1000
        )
             setDash (!dash);
             localStorage.removeItem("Admin");
        }
       if(((userid.length === 6) && idName === "SE" && 0 < parseInt(idNum)  && parseInt(idNum) <= 999  && (idNum.length ===3)) &&(pass.length === 4)  && pass === "0000"){
                        console.log("Sucess");
                        setLogin(!isLogin);
                        localStorage.setItem("usernameData",username.current.value)
       }else{
        setError(!errorMsg);
       }
    }
    
    

  return (

    <React.Fragment>
        {spinner? <ClipLoader className='spn'  color="#00f4ff" /> :admin ? <Dashboard></Dashboard>  : spinner? <ClipLoader className='spn'  color="#00f4ff" /> :dash ? <Dashboard>  </Dashboard> : <div >

        { isLogin ?  <VerifyCheck>  </VerifyCheck> :
        <form className='container' onSubmit={isVerified}>
               
                <h1>Sign In as {user ? "Adminstrator":"User"}</h1>
                
                <span className='icons'><FaUserAlt /></span>
                <input ref={username} type="text" name="username" required="req" placeholder='Username' onChange={onchange} /> <br/>
                <span className='icon2'><FaLock /></span>
                <input ref={password} type="password" name="password" required="req" placeholder='Pincode' onChange={onchange}/><br/>
              <div className='errConatiner'>
               {errorMsg ? <p>No user Exit's<span><AiFillInfoCircle /></span></p>:""}
                </div>
                <button type='submit'  className='btn' >LOGIN</button>
                <p>Or sign in using social Platforms</p>
                <div className='btnContainer'>
                <button className='btn1'> <TiSocialFacebook/></button>
                <button className='btn1 '> < RiTwitterFill /></button>
                <button className='btn1'> <AiOutlineGoogle/></button>
                <button className='btn1'> <TiSocialLinkedin/></button>
                </div>
                <p>are you a admin ? <span className='idCheck' onClick={isUser}>sign by {user ? "User" : "admin" } instead</span></p>
        </form>
}

        </div> }
    </React.Fragment>
  )
}

export default Home;